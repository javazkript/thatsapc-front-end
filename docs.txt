ThatsaPC API documentation.

== INDEX ==
1ˇ Features
2ˇ Comunication
3ˇ Types
4ˇ Options
5ˇ Commands
6ˇ Node status and reading
7ˇ License and credits

== FEATURES ==
ThatsaPC lets you to connect Whatsapp Servers, read nodes and send messages. 
ThatsaPC tries to keep connection alive, but if connection is somehow broken, we give you tools to retry connection and login.
If you are interested on adding a new feature, just contact with developers@javazkript.com

== COMUNICATION ==
ThatsaPC API is an executable that uses stdin and stdout to receive and expose information. 
That means you are able to use this API from any language capable to execute an aplication and read-write to standard pipes.
The name normally given to a bidirectional execution pipe is popen2.
The charset used to comunicate is UTF-8.
Newlines are represented by '\n' in any platform.

== TYPES ==
The information given from and to API is determined by some types. Those are:

ˇBoolean: the literal "true" or "false" (without quotes) representing two states. 
ˇInteger: A number from -2^63 to 2^63-1 as string.
ˇPhoneID: An Integer representating a phone number with country code included.
ˇReal: A float point number as string.
ˇChunk: An integer (defined above) "n" positive followed by ONE blank space and "n" bytes of raw information.
ˇString: Arbitrary sequence of non-space characters.
ˇMultiString: An integer (defined above) "n" positive followed by ONE newline ('\n') and n lines of text.

== OPTIONS == 
The API can be configured in several ways changing the value of some options:

"asynchronous" [boolean].       If true, pop will not be in blocking mode. If false, pop will block the API until a new node is received.
                                If you set up this option to true, you need to reconnect to whatsapp in order to make it work.

== COMMANDS == 

Each command have its requisites. If they are not met, normally an assert will be thrown, but
random things could happen.

ˇConnect:
Connect to WhatsApp Server. 
        -Requisite(s): To be not connected.
        -Returns a boolean, true if API could connect. False otherwise. 

ˇConnected:
Shows status of conection.
        -Requisite(s):  None.
        -Returns a boolean, true if API is connected. False otherwise. 

ˇlogin(number as PhoneID,password as String,nick as String):
Tries to login. Number is the user of the account to connect. 
Password must be IMEI if android user, or Wi-Fi MAC address if iPhone user. Nick will be shown to other people.
        -Requisite(s): To be connected.
        -Returns nothing.

ˇrelogin:
Tries to do login with the latest information given to login command.
        -Requisite(s): To be connected and to have executed (at least) once command login.
        -Returns nothing.

ˇsendmessage(msgid as String, to as PhoneID, txt as MultiString): 
Sends a text message containing "txt" to user "to". Msgid must be a valid msgid (or AUTO to generate one).
        -Requisite(s): To be connected and successfully loged in.
        -Returns nothing.

ˇrequestimageprofile(phone as PhoneID):
Sends to server a request to get the image profile of "phone" account. 
If server wants to give you that image, then you will recive a node with tag iq, and childnode picture.
        -Requisite(s): To be connected and successfully loged in.
        -Returns nothing.

ˇpop:
Changes actual node to next node in queue. To understand node system, see node status and reading.
        -Requisite(s): None (but if you are not connected and successfully loged in you will not get new nodes).
        -Returns true if there was a node in queue. False otherwise. If returned false, actual node did not changed.

ˇgettag:
Gives the tag of actual node.
        -Requisite(s): Successfully poped (at least) one node.
        -Returns String containing information.

ˇgetvalue:
Gives the value of actual node.
        -Requisite(s): Successfully poped (at least) one node.
        -Returns a Chunk representing value.

ˇgetattribute(key as String):
Gives the value of one attribute by key.
        -Requisite(s): Successfully poped (at least) one node.
        -Returns String representing value followed by newline. If attribute with that key doesn't exists, String will be empty.

ˇgochild(name as String):
Changes actual node by a child node from given name.
        -Requisite(s): Successfully poped (at least) one node.
        -Returns true if child node found. False otherwise. If false returned, actual node will not be changed.

ˇgoparent:
Changes actual node by its parent.
        -Requisite(s): Successfully poped (at least) one node.
        -Returns true if parent exists. False otherwise. If false returned, actual node will not be changed.

ˇprintnode:
Prints the actual node (debug purpsoses only).
        -Requisite(s): Successfully poped (at least) one node.
        -Returns MultiString representing the node.

ˇsendmessagereceived:
Tells WhatsApp Servers that the message represented by actual node is received.
        -Requisite(s): Successfully poped (at least) one node, and that node must have tag message.
        -Returns nothing.

ˇsetopt(key as String, value as String):
Set an API option of "key" name as "value".
        -Requisite(s): None.
        -Returns nothing.

ˇgetopt(key as String):
Get an API option of "key" name.
        -Requisite(s): None.
        -If key found, returns true followed by newline and string representing the value. If not, returns false.

ˇdisconnect:
Closes this process.
        -Requisite(s): None.
        -Returns nothing.

ˇexit:
Closes this process.
        -Requisite(s): None.
        -Returns nothing.

== NODE STATUS AND READING ==
WhatsApp send and receive XML nodes as part of XMPP protocol. If the API connected and successfully loged in to WhatsApp Server,
it will automatically receive nodes and place them into a FIFO queue. So you can pop nodes from queue in received order. When
a node is poped, it will disappear from queue and it will be "actual node". Operations gettag/value/attributes, goparent/child
and printnode uses that node.

What is a node? 

See http://en.wikipedia.org/wiki/XML

== LICENSE AND CREDITS ==
All credits of the API comes to El_Java & lilEzek (team members of Javazkript team), and contributors to whatsapi which made
this project doable. 

www.javazrkipt.com

If you are using this API successfully and you like it, please consider to donate.

License of ThatsaPC API: Attribution-NonCommercial-ShareAlike 3.0 Unported:
http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode
